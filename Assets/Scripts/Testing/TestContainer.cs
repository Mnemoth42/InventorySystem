﻿using System;
using GameDevTV.Core.UI.Dragging;
using UnityEngine;
using UnityEngine.UI;

namespace Testing
{
    public class TestContainer : MonoBehaviour, IDragContainer<Sprite>
    {
        [SerializeField] private Image image;
        [SerializeField] private Sprite sprite;
        [SerializeField] private int amount;

        private void Awake()
        {
            UpdateImage();
        }

        void UpdateImage()
        {
            image.sprite = sprite;
            image.enabled = sprite != null;
        }
        
        public int MaxAcceptable(Sprite item)
        {
            return 1;
        }

        /// <inheritdoc />
        public void AddItems(Sprite item, int number)
        {
            sprite = item;
            amount = number;
            UpdateImage();
        }

        /// <inheritdoc />
        public Sprite GetItem()
        {
            return sprite;
        }

        /// <inheritdoc />
        public int GetNumber()
        {
            return amount;
        }

        /// <inheritdoc />
        public void RemoveItems(int number)
        {
            amount--;
            if (amount <= 0)
            {
                amount = 0;
                sprite = null;
            }
            UpdateImage();
        }
    }
}