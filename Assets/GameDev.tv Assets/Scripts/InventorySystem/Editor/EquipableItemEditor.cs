﻿using System;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.InventorySystem.Editors
{
    [CustomEditor(typeof(EquipableItem))]
    public class EquipableItemEditor : InventoryItemEditor
    {
        private SerializedProperty equipLocation;

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();
            equipLocation = serializedObject.FindProperty("equipLocation");
        }

        

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUI.BeginChangeCheck();
            var location = (EquipLocation)EditorGUILayout.EnumPopup(new GUIContent("Equip Location"), (EquipLocation)equipLocation.enumValueIndex, IsValidEquipLocation, false);
            if (EditorGUI.EndChangeCheck())
            {
                equipLocation.enumValueIndex = (int)location;
                serializedObject.ApplyModifiedProperties();
            }
        }

        /// <summary>
        /// Override to disallow specific equip locations by testing the arg against any equiplocations you
        /// do not want the overridden class to be equipable at.  The default is to disallow weapons, as weapons
        /// should be WeaponConfigs... a WeaponConfig should disallow any other equip location.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        protected virtual bool IsValidEquipLocation(Enum arg)
        {
            if ((EquipLocation)arg == EquipLocation.Weapon) return false;
            return true;
        }

        protected virtual int DefaultEquipLocation => (int)EquipLocation.Head;
    }
}