﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.InventorySystem.Editors
{
    [CustomEditor(typeof(InventoryItem))]
    public class InventoryItemEditor : UnityEditor.Editor
    {
        private SerializedProperty itemID;
        private SerializedProperty displayName;
        private SerializedProperty description;
        private SerializedProperty icon;
        private SerializedProperty pickup;
        private SerializedProperty stackSize;
        private SerializedProperty basePrice;
        private InventoryItem item;

        protected virtual void OnEnable()
        {
            item = serializedObject.targetObject as InventoryItem;
            if(item==null) Debug.LogError("InventoryItem is not valid");
            itemID = serializedObject.FindProperty("itemID");
            displayName = serializedObject.FindProperty("displayName");
            description = serializedObject.FindProperty("description");
            icon = serializedObject.FindProperty("icon");
            pickup = serializedObject.FindProperty("pickup");
            stackSize = serializedObject.FindProperty("stackSize");
            basePrice = serializedObject.FindProperty("basePrice");
        }

        private static bool showBasicInventory = true;

        protected virtual bool CanStack => true;

        public void BeginIndent()
        {
            GUIStyle style = new GUIStyle();
            style.padding = new RectOffset(5, 1, 0, 0);
            EditorGUILayout.BeginVertical(style);
        }

        public void EndIndent()
        {
            EditorGUILayout.EndVertical();
        }
        
        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.HelpBox(itemID.stringValue, MessageType.None);
            if (string.IsNullOrEmpty(itemID.stringValue) ||
                GUILayout.Button("Change ID"))
            {
                itemID.stringValue = Guid.NewGuid().ToString();
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();
            showBasicInventory = EditorGUILayout.Foldout(showBasicInventory, "InventoryItem");
            if (showBasicInventory)
            {
                BeginIndent();
                
                TextField(displayName);
                SerializedProperty property = pickup;
                TextArea(description);
                EditorGUI.BeginChangeCheck();
                Sprite newIcon = (Sprite)EditorGUILayout.ObjectField("Icon",icon.objectReferenceValue, typeof(Sprite), false);
                if (EditorGUI.EndChangeCheck())
                {
                    icon.objectReferenceValue = newIcon;
                    serializedObject.ApplyModifiedProperties();
                }
                SelectMonoBehavior<Pickup>(pickup);
                if (CanStack)
                {
                    IntSlider("Stack Size", stackSize);
                }
                else
                {
                    EditorGUILayout.HelpBox("Not Stackable", MessageType.Info);
                    if (stackSize.intValue != 1)
                    {
                        stackSize.intValue = 1;
                        
                    }
                }
                IntSlider("Base Price", basePrice, 1, 1000);
                EndIndent();
            }
        }

        /// <inheritdoc />
        public override bool HasPreviewGUI()
        {
            return item!=null;
        }

        /// <inheritdoc />
        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            if (item == null) return;
            GUIStyle style = new GUIStyle();
            style.wordWrap = true;
            style.stretchHeight = true;
            style.alignment = TextAnchor.UpperCenter;
            style.fontSize = 16;
            style.richText = true;
            EditorGUI.LabelField(r,
                $"<color=#ffffff>{item.DisplayName}</color><color=#888888>\n{item.GetToolTipDescription()}</color>",
                style);
        }
        
        public void DrawTooltip(Rect r, InventoryItem item)
        {
            
        }

        /// <summary>
        /// Gets a list of assets from the asset database in Assets/Game with the given type T component.  Component must exist in the root
        /// of the prefab, this method will not find child items.  It only searchs the Assets/Game directory.  You can override this behavior
        /// by changing the path in the AssetDatabase.FindAssets function call on the 3rd line of the function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected static List<T> GetAssetsOfType<T>() where T : MonoBehaviour
        {
            List<T> itemList = new List<T>();
            string[] lGuids = AssetDatabase.FindAssets("t:GameObject", new string[] { "Assets/Game" });
            foreach (string id in lGuids)
            {
                string lAssetPath = AssetDatabase.GUIDToAssetPath(id);
                GameObject go = (GameObject)AssetDatabase.LoadAssetAtPath<GameObject>(lAssetPath);
                if (go && go.TryGetComponent(out T test))
                {
                    itemList.Add(test);
                }
            }

            return itemList;
        }
        
        /// <summary>
        /// This will take a list of items of type T and return a list of strings with their corresponding names.  Use the result
        /// of <see cref="GetAssetsOfType{T}"/> to find all objects of a given type in the Game directory for a EditorGUILayout.Popup.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<string> GetNamesFromList<T>(List<T> list) where T : MonoBehaviour
        {
            List<string> result = new List<string>();
            foreach (T go in list)
            {
                result.Add(go.name);
            }

            return result;
        }
        
        protected static void SelectMonoBehavior<T>(SerializedProperty property) where T: MonoBehaviour
        {
            List<T> items = GetAssetsOfType<T>();
            if (items.Count == 0)
            {
                EditorGUILayout.HelpBox("No Pickups In Game Directory!", MessageType.Warning);
                return;
            }
            List<string> names = GetNamesFromList(items);
            int index = items.IndexOf(property.objectReferenceValue as T);
            EditorGUI.BeginChangeCheck();
            int newIndex = EditorGUILayout.Popup(property.displayName, index, names.ToArray());
            
            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = items[newIndex];
                property.serializedObject.ApplyModifiedProperties();
            }
        }
        
        protected static void TextArea(string label, SerializedProperty property)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.LabelField(label);
            string newValue = EditorGUILayout.TextArea(property.stringValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = newValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        protected static void TextArea(SerializedProperty property)
        {
            TextArea(property.displayName, property);
        }

        protected static void TextField(string label, SerializedProperty property)
        {
            EditorGUI.BeginChangeCheck();
            string newValue = EditorGUILayout.TextField(label, property.stringValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = newValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        protected static void TextField(SerializedProperty property)
        {
            string label = property.displayName;
            TextField(label, property);
        }

        protected static void IntSlider(string label, SerializedProperty property, int min = 1, int max = 100)
        {
            EditorGUI.BeginChangeCheck();
            int newValue = EditorGUILayout.IntSlider(label, property.intValue, min, max);
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = newValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        protected static bool Button(string buttonText, string buttonTooltip="")
        {
            return GUILayout.Button(new GUIContent(buttonText, buttonTooltip));
        }
    }
}