using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.JsonSaving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.InventorySystem
{
    public class Inventory : MonoBehaviour, IJsonSaveable
    {
    #region Fields, Events, and Variables
        public struct InventorySlot
        {
            public InventoryItem item;
            public int number;
        }
        
        [SerializeField] private int inventorySize = 16;
        
        public event Action inventoryUpdated;
        public event Action<int> slotUpdated;
        
        private InventorySlot[] slots;

    #endregion
            
        
        private void Awake()
        {
            slots = new InventorySlot[inventorySize];
        }
        
    #region Helper Functions.  These methods do not alter Inventory.  
        bool IsValidSlot(int slot)
        {
            if (slot < 0 || slot >= inventorySize)
            {
                Debug.Log($"Attempting to access invalid slot {slot}, inventorySize = {inventorySize}");
                return false;
            }
            return true;
        }
        
        public int FreeSlots()
        {
            var count = 0;
            foreach (var slot in slots)
                if (slot.number == 0)
                    count++;
            return count;
        }
        
        public InventoryItem GetItemInSlot(int slot)
        {
            return IsValidSlot(slot)?slots[slot].item:null;
        }

        public int GetNumberInSlot(int slot)
        {
            return IsValidSlot(slot)?slots[slot].number:0;
        }
        
        public bool HasItem(InventoryItem item)
        {
            for (var i = 0; i < slots.Length; i++)
                if (ReferenceEquals(slots[i].item, item))
                    return true;
            return false;
        }
        

        
        private int FindStack(InventoryItem item)
        {
            if (!item.IsStackable) return -1;

            for (var i = 0; i < slots.Length; i++)
                if (ReferenceEquals(slots[i].item, item))
                    return i;
            return -1;
        }
        
        private int FindEmptySlot()
        {
            for (var i = 0; i < slots.Length; i++)
                if (slots[i].item == null)
                    return i;
            return -1;
        }
    #endregion
        
    #region Actions
        public void RemoveFromSlot(int slot, int number)
        {
            if (!IsValidSlot(slot)) return;
            slots[slot].number -= number;
            if (slots[slot].number <= 0)
            {
                slots[slot].number = 0;
                slots[slot].item = null;
            }

            inventoryUpdated?.Invoke();
        }
    #endregion
        
    #region Saving -- JToken
        
        public JToken CaptureAsJToken()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            for (int i = 0; i < inventorySize; i++)
            {
                if (slots[i].item == null || slots[i].number <= 0) continue;
                JObject item = new JObject();
                IDictionary<string, JToken> itemDict = item;
                itemDict["id"] = slots[i].item.ItemID;
                itemDict["data"] = slots[i].item.CaptureAsJToken();
                itemDict["number"] = slots[i].number;
                stateDict[$"{i}"] = item;
            }
            return state;
        }

       
        public void RestoreFromJToken(JToken state)
        {
            IDictionary<string, JToken> stateDict = (JObject)state;
            for (int i = 0; i < inventorySize; i++)
            {
                if (stateDict.ContainsKey($"{i}"))
                {
                    IDictionary<string, JToken> itemDict = (JObject)stateDict[$"{i}"];
                    InventoryItem item = InventoryItem.GetFromID(itemDict["id"].ToString());
                    if (item != null)
                    {
                        item.RestoreFromJToken(itemDict["data"].ToString());
                        int number = itemDict["number"].ToObject<int>();
                        slots[i] = new InventorySlot() { item = item, number = number };
                    }
                    else
                    {
                        slots[i] = new InventorySlot();
                    }
                }
                else
                {
                    slots[i] = new InventorySlot();
                }
            }
            inventoryUpdated?.Invoke();
        }
    #endregion

       
    }
}
