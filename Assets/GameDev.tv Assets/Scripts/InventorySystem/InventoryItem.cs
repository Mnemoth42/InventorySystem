using System.Collections.Generic;
using System.Text;
using GameDevTV.JsonSaving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.InventorySystem
{
    [CreateAssetMenu(fileName = "New InventoryItem", menuName = "Inventory/New InventoryItem")]
    public class InventoryItem : ScriptableObject, IJsonSaveable
    {
        [SerializeField] private string itemID = "";
        [SerializeField] private string displayName = "";
        [SerializeField] private string description = "";
        [SerializeField] private Sprite icon;
        [SerializeField] private Pickup pickup;
        [SerializeField] private int stackSize = 1;
        [SerializeField] private int basePrice = 10;

        public string ItemID => itemID;
        public string DisplayName => displayName;
        public string Description => description;
        public Sprite Icon => icon;
        public Pickup Pickup => pickup;
        public int StackSize => stackSize;
        public bool IsStackable => stackSize>1;

        /// <summary>
        /// Override this method to make modifications to the price.  For example, an
        /// item with random stats might raise the price based on the number of stat boosts.
        /// </summary>
        /// <returns></returns>
        public virtual int GetPrice() => basePrice;
        /// <summary>
        /// Override this in child classes to add details
        /// like stat buffs to the tooltip.
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildToolTipDescription()
        {
            return description;
        }
        /// <summary>
        /// This method will return the formatted description for the tooltip.
        /// Subclasses should add details by overriding BuildToolTipDescription().
        /// This will put BuildToolTipDescription together with inventory item details that
        /// should be at the bottom of the tooltip.
        /// </summary>
        /// <returns></returns>
        public string GetToolTipDescription()
        {
            StringBuilder builder = new StringBuilder(BuildToolTipDescription());
            if (IsStackable)
            {
                builder.Append($"\nStackable ({stackSize})");
            }
            builder.Append($"\nMarket Price: {GetPrice()}");
            return builder.ToString();
        }

        private static Dictionary<string, InventoryItem> itemLookup;
        public static InventoryItem GetFromID(string id)
        {
            if (itemLookup == null)
            {
                itemLookup = new();
                foreach (InventoryItem item in Resources.LoadAll<InventoryItem>(""))
                {
                    if (itemLookup.ContainsKey(item.itemID))
                    {
                        Debug.LogWarning($"Inventory Item {item.name} has the same ItemID as {itemLookup[item.itemID].name}");
                    }
                    itemLookup[item.itemID] = item;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                Debug.LogWarning("InventoryItem.GetFromID(): Cannot look up a blank or null itemID");
                return null;
            }

            if (itemLookup.ContainsKey(id)) return itemLookup[id];
            Debug.LogWarning($"InventoryItem.GetFromID() no item in a Resources folder has an ID of {id}. Make sure your InventoryItems are in a folder named Resources");
            return null;
        }
        
        
        public virtual JToken CaptureAsJToken()
        {
            return new JObject();
        }

        
        public virtual void RestoreFromJToken(JToken state)
        {
            
        }
    }
}