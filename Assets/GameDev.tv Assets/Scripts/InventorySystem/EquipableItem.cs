﻿using UnityEngine;

namespace GameDevTV.InventorySystem
{
    public enum EquipLocation
    {
        Head,
        Neck,
        Chest,
        Shoulders,
        Arms,
        Hands,
        Weapon,
        Shield,
        Ring
    }
    
    [CreateAssetMenu(fileName = "EquipableItem", menuName = "Inventory/Equipable Item", order = 0)]
    public class EquipableItem : InventoryItem
    {
        [SerializeField] private EquipLocation equipLocation;
    }
}