using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace GameDevTV.Core.UI.Dragging
{
    public abstract class DraggingSystem<T> : MonoBehaviour, DragInput.IDraggingActions where T:class
    {
        

        private Vector2 currentScreenPosition;
        private DragItem<T> draggingObject;
        private Transform originalParent;
        private Vector2 originalPosition;
        private Canvas canvas;
        private DragInput input;
        private void Awake()
        {
            input = new DragInput();
            input.Dragging.SetCallbacks(this);
            canvas = GetComponent<Canvas>();
        }

        private void OnEnable()
        {
            input.Dragging.Enable();
        }

        private void OnDisable()
        {
            input.Dragging.Disable();
        }

        private void EndDrag()
        {
            StopAllCoroutines();
            if (draggingObject == null) return;
            draggingObject.transform.position = originalPosition;
            draggingObject.transform.SetParent(originalParent, true); 
            var destination = GetDestinationAtCurrentScreenPosition();
            if (destination != null)
            {
                draggingObject.DropItemIntoContainer(destination);
            }
            draggingObject = null;
        }

        private void BeginDrag()
        {
            draggingObject = GetDragItemAtCurrentScreenPosition();
            if (draggingObject == null) return;
            StartCoroutine(Drag());
        }

        IEnumerator Drag()
        {
            if (draggingObject == null) yield break;
            IDragSource<T> source = draggingObject.GetComponentInParent<IDragSource<T>>();
            if (source == null || source.GetItem()==null || source.GetNumber()<=0)
            {
                draggingObject = null;
                yield break;
            }
            originalPosition = draggingObject.transform.position;
            originalParent = draggingObject.transform.parent;
            draggingObject.transform.SetParent(canvas.transform, true);
            while (draggingObject != null)
            {
                draggingObject.transform.position = currentScreenPosition;
                yield return null;
            }
        }

        private void ScreenPositionOnPerformed(InputAction.CallbackContext context)
        {
            currentScreenPosition = context.ReadValue<Vector2>();
        }

        private DragItem<T> GetDragItemAtCurrentScreenPosition()
        {
            var results = RaycastEventSystemAtCurrentScreenPosition();
            foreach (RaycastResult result in results)
            {
                if (result.gameObject.TryGetComponent(out DragItem<T> dragItem))
                {
                    return dragItem;
                }
            }
            return null;
        }

        private List<RaycastResult> RaycastEventSystemAtCurrentScreenPosition()
        {
            var eventData = new PointerEventData(EventSystem.current);
            eventData.position = currentScreenPosition;
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, results);
            return results;
        }

        private IDragDestination<T> GetDestinationAtCurrentScreenPosition()
        {
            var results = RaycastEventSystemAtCurrentScreenPosition();
            foreach (RaycastResult raycastResult in results)
            {
                if (raycastResult.gameObject.TryGetComponent(out IDragDestination<T> destination))
                {
                    return destination;
                }
            }
            return null;
        }

        
        public void OnPress(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                BeginDrag();
            }
            else if(context.canceled)
            {
                EndDrag();
            }
        }

        
        public void OnScreenPosition(InputAction.CallbackContext context)
        {
            currentScreenPosition = context.ReadValue<Vector2>();
        }
        
    }
}